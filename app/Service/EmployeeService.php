<?php


namespace App\Service;


use App\Models\Employee;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class EmployeeService
{
    public function updateAllSubordination($boss, $subordinates)
    {
        foreach ($subordinates as $subordinate) {
            $subordinate = $this->reCalculateAboveLevel($boss, $subordinate);
            if ($subordinate->subordinationEmployees->count() > 0) {
                $this->updateAllSubordination($subordinate, $subordinate->subordinationEmployees()->get());
            }
        }
    }

    public function reCalculateAboveLevel($boss, $subordinate)
    {
        $subordinate->levels_above = ($boss->levels_above + 1);
        $subordinate->save();
        return $subordinate;
    }

    public function countLevelOfSubordination($employee)
    {
        $subEmployees = $employee->subordinationEmployees()->get();
        $levelOfSub = 0;
        if ($subEmployees->isEmpty() || $subEmployees->count() == 0) {
            return null;
        } else {
            foreach ($subEmployees as $subordinates) {
                $levelOfSub = max($levelOfSub, $this->countLevelOfSubordination($subordinates));
            }
            return $levelOfSub + 1;
        }
    }

    public function updateLevelUnder($employee)
    {
        $employee->levels_under = $this->countLevelOfSubordination($employee);
        $employee->save();

        if (isset($employee->boss_id)) {
            $boss = Employee::findOrFail($employee->boss_id);
            if (($boss->levels_under - $employee->levels_under) == 1) {
                $employee->levels_under = $this->countLevelOfSubordination($employee);
                $employee->save();
                $this->updateLevelUnder($boss);
            }
        }

    }

    private function preparePhoneForDb($phone)
    {
        $withOneBracket = mb_substr(str_replace(" ", "", $phone), 5);
        return str_replace(")", "", $withOneBracket);
    }

    public function save($data)
    {
        if (isset($data['photo'])){
            $data['photo'] = Image::make($data['photo'])
                ->orientate()
                ->fit(300)
                ->save('storage/photos/' . uniqid() . '.jpg', 80)
                ->basePath();
        }

        $boss = Employee::where('id', $data['boss_id'])->first();

        $data['phone'] = $this->preparePhoneForDb($data['phone']);
        $data['admin_created_id'] = Auth::user()->id;
        $data['admin_updated_id'] = Auth::user()->id;
        $data['levels_above'] = $boss->levels_above + 1;
        Employee::firstOrCreate($data);
        $this->updateLevelUnder($boss);

    }

    public function update($data, $employee)
    {

        if (isset($data['photo'])) {
            $data['photo'] = Image::make($data['photo'])
                ->orientate()
                ->fit(300)
                ->save('storage/photos/' . uniqid() . '.jpg', 80)
                ->basePath();
        }

        $data['phone'] = $this->preparePhoneForDb($data['phone']);
        $data['admin_updated_id'] = Auth::user()->id;

        if (isset($data['boss_id'])){
            $newBoss = Employee::where('id', $data['boss_id'])->firstOrFail();
            $data['levels_above'] = $newBoss->levels_above + 1;
            $this->updateLevelUnder($newBoss);
            $employee->boss_id = $data['boss_id'];
        }

        if (isset($employee->boss_id)){
            $oldBoss = Employee::where('id', $employee->boss_id)->firstOrFail();
            $this->updateLevelUnder($oldBoss);
        }
        $employee->update($data);

        if ($this->countLevelOfSubordination($employee) != 0) {
            $subordinations = $employee->subordinationEmployees()->get();
            $this->updateAllSubordination($employee, $subordinations);
        }
    }

    public function delete(Employee $employee, Employee $bossForSubordinates = null)
    {
        $oldBoss = Employee::where('id', $employee->boss_id)->first();

        if ($this->countLevelOfSubordination($employee) != 0) {
            $subordinations = $employee->subordinationEmployees()->get();
            $this->updateAllSubordination($employee, $subordinations);
            if (isset($bossForSubordinates)) {
                foreach ($subordinations as $subordination) {
                    $subordination->boss_id = $bossForSubordinates->id;
                    $subordination->levels_above = $bossForSubordinates->levels_above + 1;
                    $subordination->save();
                    $this->updateAllSubordination($subordination, $subordination->subordinationEmployees()->get());
                }
                $employee->delete();
                $this->updateLevelUnder($bossForSubordinates);
            } else {
                foreach ($subordinations as $subordination) {
                    $subordination->boss_id = null;
                    $subordination->levels_above = null;
                    $subordination->save();
                    $this->updateAllSubordination($subordination, $subordination->subordinationEmployees()->get());
                }
                $employee->delete();
            }
        }
        if (isset($oldBoss)) {
            $this->updateLevelUnder($oldBoss);
        }
    }
}
