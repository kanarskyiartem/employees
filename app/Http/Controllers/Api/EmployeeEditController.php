<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Admin\Employee\BaseController;
use App\Http\Resources\Employee\EmployeeResource;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeEditController extends BaseController
{
    public function __invoke(Request $request)
    {
        $search = $request->search;
        $id = $request->id;

        $employees = Employee::where('name', 'like', '%' . $search . '%')
            ->orWhereNull('levels_above')
            ->where('levels_above', '<', self::MAX_SUBORDINATE_LEVEL)
            ->orderBy('name', 'asc')
            ->select('id', 'name')
            ->limit(7)
            ->get()
            ->where('id', '!=', $id);

        return EmployeeResource::collection($employees);
    }
}
