<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Admin\Employee\BaseController;
use App\Http\Resources\Employee\EmployeeResource;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeCreateController extends BaseController
{
    public function __invoke(Request $request)
    {
        $search = $request->search;

        if ($search === '') {
            $employees = Employee::where('levels_above', '<', self::MAX_SUBORDINATE_LEVEL)
                ->orWhereNull('levels_above')
                ->orderBy('name', 'asc')
                ->select('id', 'name')
                ->limit(7)
                ->get();
        } else {
            $employees = Employee::where('name', 'like', '%' . $search . '%')
                ->orWhereNull('levels_above')
                ->where('levels_above', '<', self::MAX_SUBORDINATE_LEVEL)
                ->orderBy('name', 'asc')
                ->select('id', 'name')
                ->limit(7)
                ->get();
        }
        return EmployeeResource::collection($employees);
    }
}
