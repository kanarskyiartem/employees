<?php

namespace App\Http\Controllers\Admin\Position;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Position\StoreRequest;
use App\Models\Position;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $data['admin_created_id'] = Auth::user()->id;
        $data['admin_updated_id'] = Auth::user()->id;
        Position::firstOrCreate($data);
        return redirect()->route('admin.position.index');
    }
}
