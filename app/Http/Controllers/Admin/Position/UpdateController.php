<?php

namespace App\Http\Controllers\Admin\Position;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Position\UpdateRequest;
use App\Models\Position;
use Illuminate\Support\Facades\Auth;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Position $position)
    {
        $data = $request->validated();
        $data['admin_updated_id'] = Auth::user()->id;
        $position->update($data);
        return redirect(route('admin.position.index'));
    }
}
