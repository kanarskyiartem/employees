<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Requests\Admin\Employee\StoreRequest;

class StoreController extends BaseController
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $this->service->save($data);

        return redirect()->route('admin.employee.index');
    }
}
