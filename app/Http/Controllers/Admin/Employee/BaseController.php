<?php


namespace App\Http\Controllers\Admin\Employee;


use App\Http\Controllers\Controller;
use App\Service\EmployeeService;
use Illuminate\Support\Facades\Response;

class BaseController extends Controller
{
    public $service;

    const MAX_SUBORDINATE_LEVEL = 5;

    public function __construct(EmployeeService $service)
    {
        $this->service = $service;
    }

    public function sendSuccess($message)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }
}
