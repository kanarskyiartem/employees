<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Models\Employee;
use App\Models\Position;

class EditController extends BaseController
{

    public function __invoke(Employee $employee)
    {
        $positions = Position::all();
        return view('admin.employee.edit', compact('positions', 'employee'));
    }
}
