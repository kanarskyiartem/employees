<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Models\Position;

class CreateController extends BaseController
{
    public function __invoke()
    {
        $positions = Position::all();
        return view('admin.employee.create', compact('positions'));
    }
}
