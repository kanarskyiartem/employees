<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Models\Employee;
use Illuminate\Http\Request;

class DeleteController extends BaseController
{
    public function __invoke(Employee $employee, Request $request)
    {
        if (isset($request->boss_id)) {
            $bossForSubordinates = Employee::find($request->boss_id)->first();
            $this->service->delete($employee, $bossForSubordinates);
        }else{
            $this->service->delete($employee);
        }
        return $this->sendSuccess("Employee deleted");
    }
}
