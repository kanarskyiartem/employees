<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Queries\EmployeeDatatable;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class IndexController extends BaseController
{
    public function __invoke(Request $request)
    {
        if ($request->ajax()) {
            return Datatables::of((new EmployeeDatatable())->get()->with('position'))
                ->addIndexColumn()
                ->editColumn('photo', function ($data) {
                    return '<img style="height: auto; width: 40px; border-radius: 40px" src="'
                        . asset($data->photo)
                        . '" alt="">';
                })
                ->editColumn('date_of_employment', function ($data) {
                    return with($data)->dateOfEmployment();
                })
                ->editColumn('phone', function ($data) {
                    return with($data)->getFormattedPhoneNumber();
                })
                ->rawColumns(['action', 'photo'])
                ->make(true);
        }
        return view('admin.employee.index');
    }
}
