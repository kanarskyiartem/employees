<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:256',
            'photo' => 'nullable|image|mimes:png,jpg|max:5120|dimensions:min_width=300,min_height=300',
            'phone' => ['required', 'regex:/^\+380[\s|(]{1,2}[0-9]{2}[\s|)]{1}[\s]{1}[0-9]{3}([\s]{1}[0-9]{2}){2}/'],
            'email' => 'required|unique:employees,email,' . $this->employee_id,
            'position_id' => 'required|exists:positions,id',
            'salary' => 'required|regex:/^\d+(\.\d{1,2})?$/|max:500000',
            'boss_id' => 'nullable|exists:employees,id',
            'date_of_employment' => 'required|date',
        ];
    }
}
