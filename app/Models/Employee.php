<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $guarded = false;

    public function dateOfEmployment()
    {
        return Carbon::parse($this->date_of_employment)->format('d.m.y');
    }

    public function dateOfEmploymentForInput()
    {
        return Carbon::parse($this->date_of_employment)->format('Y-m-d');
    }

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function boss()
    {
        return $this->belongsTo(Employee::class, 'boss_id', 'id');
    }

    public function subordinationEmployees()
    {
        return $this->hasMany(Employee::class, 'boss_id', 'id');
    }

    public function getFormattedPhoneNumber()
    {
        $phoneNumber = '+380 ('
            . substr($this->phone, 0, 2) . ') '
            . substr($this->phone, 2, 3) . ' '
            . substr($this->phone, 5, 2) . ' '
            . substr($this->phone, 7, 2);

        return $phoneNumber;
    }
}
