<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use HasFactory;

    protected $guarded = false;

    function lastUpdatedDate(){
        return Carbon::parse($this->updated_at)->format('d.m.y');
    }
}
