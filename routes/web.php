<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::group(['namespace' => 'Position', 'prefix' => 'positions'], function () {
        Route::get('/', 'IndexController')->name('admin.position.index');
        Route::get('/create', 'CreateController')->name('admin.position.create');
        Route::post('/', 'StoreController')->name('admin.position.store');
        Route::get('/{position}', 'ShowController')->name('admin.position.show');
        Route::get('/{position}/edit', 'EditController')->name('admin.position.edit');
        Route::patch('/{position}/', 'UpdateController')->name('admin.position.update');
        Route::delete('/{position}/', 'DeleteController')->name('admin.position.delete');
    });
    Route::group(['namespace' => 'Employee', 'prefix' => 'employees'], function () {
        Route::get('/', 'IndexController')->name('admin.employee.index');
        Route::get('/create', 'CreateController')->name('admin.employee.create');
        Route::post('/', 'StoreController')->name('admin.employee.store');
        Route::get('/{employee}', 'ShowController')->name('admin.employee.show');
        Route::get('/{employee}/edit', 'EditController')->name('admin.employee.edit');
        Route::patch('/{employee}', 'UpdateController')->name('admin.employee.update');
        Route::delete('/{employee}', 'DeleteController')->name('admin.employee.delete');
    });
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);
