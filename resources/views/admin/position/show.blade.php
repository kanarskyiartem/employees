@extends('adminlte::page')

@section('content_header')
    <div class="d-inline-block">
        <h1>Positions</h1>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Position {{$position->name}}</h3>


                </div>
                <div class="card-body table-responsive p-0">
                    <table class="table table-striped">
                        <colgroup>
                            <col class="col-md-9">
                            <col class="col-md-3">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Last update</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$position->name}}</td>
                                <td>{{$position->updated_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
