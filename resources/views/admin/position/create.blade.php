@extends('adminlte::page')
@section('content_header')
    <div class="d-inline-block">
        <h1>Positions</h1>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-5">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Add position</h3>
                </div>
                <form action="{{route('admin.position.store')}}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            @error('name')<i class="text-gray far fa-times-circle"></i> @enderror
                            <label for="inputNamePosition">Name</label>
                            <input type="text" class="form-control" id="inputNamePosition" name="name"
                                   placeholder="Enter name of position"
                                   onkeyup="countChars('inputNamePosition','restriction');">
                            @error('name')
                            <div class="text-danger text-left" >
                                {{$message}}
                            </div>
                            @enderror
                            <p style="display:block; text-align:right" class="text-secondary" id="restriction">0/256</p>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="reset" onclick="resetRestriction('restriction')"
                                class="col-4 btn border border-secondary">Cancel
                        </button>
                        <button type="submit" class="col-4 btn btn-secondary border border-secondary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@stop
@section('js')
    <script>
        const maxLengthOfName = 256;

        function countChars(countfrom, restriction) {
            let len = document.getElementById(countfrom).value.length;
            document.getElementById(restriction).innerHTML = len + '/' + maxLengthOfName;
            if (len > maxLengthOfName) {
                moreThenRestriction(restriction);
            } else {
                lessThenRestriction(restriction);
            }
        }

        function moreThenRestriction(restriction) {
            document.getElementById(restriction).classList.add('text-danger');
        }

        function lessThenRestriction(restriction) {
            document.getElementById(restriction).classList.remove('text-danger');
        }

        function resetRestriction(restriction) {
            lessThenRestriction(restriction);
            document.getElementById(restriction).innerHTML = '0/256';
        }
    </script>
@stop
