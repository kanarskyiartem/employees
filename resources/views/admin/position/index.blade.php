@extends('adminlte::page')

@section('content_header')
    <div class="d-inline-block">
        <h1>Positions</h1>
    </div>
    <div class="d-inline-block float-right">
        <a href="{{route('admin.position.create')}}" type="button" class="btn btn-block btn-secondary">
            Add position
        </a>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title">Position list</h1>
                </div>
                <div class="mt-3 ml-3" style="width: 95%">
                    <table id="myTable" class="table table-striped m-3" >
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Last update</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($positions as $position)
                            <tr>
                                <td>{{$position->name}}</td>
                                <td>{{$position->lastUpdatedDate()}}</td>
                                <td class="text-center">
                                    <a href="{{route('admin.position.edit', $position->id)}}">
                                        <i class="text-gray fas fa-pencil-alt"></i>
                                    </a>
                                    <a type="submit" class="border-0 bg-transparent" data-toggle="modal"
                                       data-target="#modalDelete{{$position->id}}">
                                        <i class="text-gray fas fa-trash" role="button"></i>
                                    </a>
                                </td>
                                @include('admin.position.modal.delete')
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>
@stop

