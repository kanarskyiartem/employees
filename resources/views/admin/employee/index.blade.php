@extends('adminlte::page')

@section('content_header')
    <div class="d-inline-block">
        <h1>Employees</h1>
    </div>
    <div class="d-inline-block float-right">
        <a href="{{route('admin.employee.create')}}" type="button" class="btn btn-block btn-secondary">
            Add employee
        </a>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">Employee list</h2>
                </div>
                <div class="card-body table-responsive p-0">
                    <div class="mt-3 ml-3" style="width: 95%">
                        <table id="myTable" class="table table-striped m-3">
                            <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Date of employment</th>
                                <th>Phone number</th>
                                <th>Email</th>
                                <th>Salary</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Remove employee</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span id="close" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to remove employee <b id="delete-employee"></b>?
                </div>
                <div class="form-group ml-2">
                    <label for="inputPosition">Select new Head for employees</label>
                    <select id="boss_id" class="form-control js-example-basic-single col-3"
                            multiple="multiple" data-maximum-selection-length="1">
                    </select>
                </div>
                <div class="modal-footer">
                    <button id="cancel_button" type="button" class="btn border-secondary" data-dismiss="modal">Close
                    </button>
                    <button id="ok_button" type="button" class="btn btn-secondary">Remove</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        const employeeUrl = '{{ route('admin.employee.index') }}';
        $(document).ready(function () {
            $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: employeeUrl,
                columns: [
                    {data: 'photo', name: 'photo', orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'position.name', name: 'position.name', defaultContent: "<i>Not set</i>"},
                    {data: 'date_of_employment', name: 'date_of_employment'},
                    {data: 'phone', name: 'phone'},
                    {data: 'email', name: 'email'},
                    {data: 'salary', name: 'salary'},
                    {
                        data: function (row) {
                            return '<a href="' + employeeUrl + '/' + row.id + '/edit">' +
                                '<i class="text-gray fas fa-pencil-alt"></i>' +
                                '</a>' +
                                '<a class="btn action-btn btn-sm delete-btn" data-id="' + row.id + '" data-name="' + row.name + '">' +
                                '<i class="text-gray fas fa-trash">' +
                                '</a>';
                        }, name: 'action', orderable: false, searchable: false
                    },
                ],
            });
        });

        $(document).on('click', '.delete-btn', function (event) {
            const id = $(event.currentTarget).data('id');
            const name = $(event.currentTarget).data('name');
            $('.js-example-basic-single').select2({
                ajax: {
                    url: "{{route('getListOfNewBossForEmployees')}}",
                    type: 'GET',
                    DataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term,
                            id: id
                        }
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                },
                allowClear: true,
                maximumSelectionLength: 1,
            });
            $('#delete-employee').text(name);
            $('#modal').show();
            $('#ok_button').click(function () {
                $.ajax({
                    url: employeeUrl + '/' + id,
                    type: 'DELETE',
                    DataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "boss_id": $('#boss_id').val()
                    },
                    success: function (response) {
                        $('#modal').hide();
                        $('#myTable').DataTable().ajax.reload(null, false);
                    },
                });
            })
            $('#cancel_button').click(function () {
                $('#modal').hide();
            })
            $('#close').click(function () {
                $('#modal').hide();
            })
        })
    </script>
@stop
