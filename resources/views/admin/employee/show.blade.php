@extends('adminlte::page')

@section('content_header')
    <div class="d-inline-block">
        <h1>Positions</h1>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Employee <b>{{$employee->name}}</b></h3>


                </div>
                <div class="card-body table-responsive p-0">
                    <table class="table table-striped">
                        <colgroup>
                            <col class="col-md-1">
                            <col class="col-md-1">
                            <col class="col-md-2">
                            <col class="col-md-2">
                            <col class="col-md-2">
                            <col class="col-md-2">
                            <col class="col-md-2">
                            <col class="col-md-1">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Photo</th>
                            <th>Position</th>
                            <th>Boss</th>
                            <th>Date of employment</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Salary</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$employee->name}}</td>
                                <td><img style="height: auto; width: 40px; border-radius: 40px" src="{{asset($employee->photo)}}" alt=""></td>
                                <td>{{$employee->position->name}}</td>
                                @isset($employee->boss)
                                <td>{{$employee->boss->name}}</td>
                                @else
                                    <td>Don't have a boss</td>
                                @endisset
                                <td>{{$employee->date_of_employment}}</td>
                                <td>{{$employee->getFormattedPhoneNumber()}}</td>
                                <td>{{$employee->email}}</td>
                                <td>{{$employee->salary}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
