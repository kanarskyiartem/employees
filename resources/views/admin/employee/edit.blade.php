@extends('adminlte::page')
@section('content_header')
    <div class="d-inline-block">
        <h1>Employees</h1>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-5">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Employee edit</h3>
                </div>
                <form action="{{route('admin.employee.update', $employee->id)}}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="card-body">

                        <div class="form-group">
                            @error('photo')<i class="text-gray far fa-times-circle"></i>@enderror
                            <label for="formFile">Photo</label>
                            <div class="mb-3">
                                <img src="{{asset($employee->photo)}}" alt="">
                            </div>
                            <div>
                                <span class="btn border-secondary btn-file col-4">
                                Browse <input type="file" name="photo">
                            </span>
                            </div>
                            @error('photo')
                            <div class="text-danger text-left">
                                {{$message}}
                            </div>
                            @enderror
                            <p style="display:block;" class="text-secondary">File format jpg,png up to 5MB, the minimum
                                size of 300x300px</p>
                        </div>

                        <div class="form-group">
                            @error('name')<i class="text-gray far fa-times-circle"></i>@enderror
                            <label for="inputName">Name</label>
                            <input type="text" class="form-control" id="inputName" name="name"
                                   placeholder="Enter name of position" value="{{$employee->name}}"
                                   onkeyup="countChars('inputName','restriction');">
                            @error('name')
                            <div class="text-danger text-left">
                                {{$message}}
                            </div>
                            @enderror
                            <p style="display:block; text-align:right" class="text-secondary" id="restriction">0/256</p>
                        </div>

                        <div class="form-group">
                            @error('phone')<i class="text-gray far fa-times-circle"></i>@enderror
                            <label for="inputPhone">Phone</label>
                            <input type="text" class="form-control" id="inputPhone" name="phone"
                                   value="{{$employee->getFormattedPhoneNumber()}}">
                            @error('phone')
                            <div class="text-danger text-left">
                                {{$message}}
                            </div>
                            @enderror
                            <p style="display:block; text-align:right" class="text-secondary" id="restriction">Required
                                format +380 (xx) XXX XX XX</p>
                        </div>

                        <div class="form-group">
                            @error('email')<i class="text-gray far fa-times-circle"></i>@enderror
                            <label for="inputEmail">Email</label>
                            <input type="email" class="form-control" id="inputEmail" name="email"
                                   value="{{$employee->email}}">
                            @error('email')
                            <div class="text-danger text-left">
                                {{$message}}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            @error('position_id')<i class="text-gray far fa-times-circle"></i>@enderror
                            <label for="inputPosition">Position</label>
                            <select name="position_id" class="form-control">
                                @foreach($positions as $position)
                                    <option
                                        value="{{$position->id}}"
                                        {{$position->id == $employee->position_id ? 'selected' :''}}
                                    >{{$position->name}}</option>
                                @endforeach
                            </select>
                            @error('position')
                            <div class="text-danger text-left">
                                {{$message}}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            @error('salary')<i class="text-gray far fa-times-circle"></i>@enderror
                            <label for="inputSalary">Salary, $</label>
                            <input type="number" step="0.01" class="form-control" id="inputSalary" name="salary"
                                   value="{{$employee->salary}}">
                            @error('salary')
                            <div class="text-danger text-left">
                                {{$message}}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            @error('boss_id')<i class="text-gray far fa-times-circle"></i>@enderror
                            <label for="inputBoss">Head</label>
                            <div>
                                <select class="form-control js-example-basic-single col-8" name="boss_id"
                                        multiple="multiple" data-maximum-selection-length="1">
                                    @if(isset($employee->boss))
                                    <option value="{{$employee->boss_id}}" selected >{{$employee->boss->name}}</option>
                                    @else
                                        <option value="0">Select one</option>
                                    @endif
                                </select>
                            </div>
                            @error('boss_id')
                            <div class="text-danger text-left">
                                {{$message}}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            @error('date_of_employment')<i class="text-gray far fa-times-circle"></i>@enderror
                            <label for="inputDateOfEmployment">Date of employment</label>
                            <input type="date" class="form-control" id="inputDateOfEmployment"
                                   name="date_of_employment" value="{{$employee->dateOfEmploymentForInput()}}">
                            @error('date_of_employment')
                            <div class="text-danger text-left">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group" style="display: inline-block">
                            <p><b>Created at: </b>{{$employee->created_at}}</p>
                            <p><b>Updated at: </b>{{$employee->updated_at}}</p>
                        </div>
                        <div style="display: inline-block; float: right;">
                            <p><b>Admin created id: </b>{{$employee->admin_created_id}}</p>
                            <p><b>Admin updated id: </b>{{$employee->admin_updated_id}}</p>
                        </div>
                        <div class="form-group w-50">
                            <input type="hidden" name="employee_id" value="{{$employee->id}}">
                        </div>

                    </div>
                    <div class="card-footer text-right">
                        <button type="reset" onclick="resetRestriction('restriction')"
                                class="col-4 btn border border-secondary">Cancel
                        </button>
                        <button type="submit" class="col-4 btn btn-secondary border border-secondary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        const maxLengthOfName = 256;

        document.addEventListener("DOMContentLoaded", function () {
            countChars('inputName', 'restriction');

        });

        $(document).ready(function () {
            $('.js-example-basic-single').select2({
                    ajax: {
                        url: '{{route('getForEditEmployees')}}',
                        type: 'GET',
                        DataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                id: {{$employee->id}}
                            }
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data.data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }
                                })
                            };
                        },
                    },
                    allowClear: true,
                    maximumSelectionLength: 1,
                });
        });

        function countChars(countfrom, restriction) {
            let len = document.getElementById(countfrom).value.length;
            document.getElementById(restriction).innerHTML = len + '/' + maxLengthOfName;
            if (len > maxLengthOfName) {
                moreThenRestriction(restriction);
            } else {
                lessThenRestriction(restriction);
            }
        }

        function moreThenRestriction(restriction) {
            document.getElementById(restriction).classList.add('text-danger');
        }

        function lessThenRestriction(restriction) {
            document.getElementById(restriction).classList.remove('text-danger');
        }

        function resetRestriction(restriction) {
            lessThenRestriction(restriction);
            document.getElementById(restriction).innerHTML = '0/256';
        }
    </script>
@stop
