<div class="modal fade" id="modalDelete{{$employee->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <form action="{{route('admin.employee.delete', $employee->id)}}" method="POST">
        @csrf
        @method("DELETE")
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Remove position</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to remove employee <b>{{$employee->name}}</b>?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn border-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-secondary">Remove</button>
                </div>
            </div>
        </div>
    </form>
</div>
