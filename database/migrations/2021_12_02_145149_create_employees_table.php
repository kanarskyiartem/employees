<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('photo')->nullable();
            $table->string('name');
            $table->unsignedBigInteger('position_id')->nullable();
            $table->unsignedBigInteger('boss_id')->nullable();
            $table->timestamp('date_of_employment');
            $table->unsignedInteger('phone');
            $table->string('email');
            $table->double('salary');
            $table->unsignedBigInteger('admin_created_id');
            $table->unsignedBigInteger('admin_updated_id');
            $table->unsignedSmallInteger('levels_above')->nullable();
            $table->unsignedSmallInteger('levels_under')->nullable();
            $table->timestamps();

            $table->index('name', 'employee_idx');
            $table->foreign('admin_created_id', 'employee_user_created_fk')
                ->on('users')
                ->references('id');
            $table->foreign('admin_updated_id', 'employee_user_updated_fk')
                ->on('users')
                ->references('id');
            $table->foreign('boss_id', 'user_user_fk')
                ->on('employees')
                ->references('id');
            $table->foreign('position_id', 'employee_position_fk')
                ->on('positions')
                ->references('id')
                ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
