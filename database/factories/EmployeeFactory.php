<?php

namespace Database\Factories;

use App\Models\Employee;
use App\Models\Position;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::all();
        $positions = Position::all();

        return [
            'photo' => 'storage/photos/id-photo2.jpg',
            'name' => $this->faker->unique()->name(),
            'position_id' => $positions->random()->id,
            'boss_id' => Employee::all()->isEmpty() || $this->faker->boolean(5) ? null : Employee::all()->random()->id,
            'date_of_employment'=> $this->faker->dateTimeBetween('-4 years', '-2 years'),
            'phone'=> $this->faker->unique()->randomNumber(9,true),
            'email' => $this->faker->unique()->safeEmail(),
            'salary' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 500000),
            'admin_created_id' => $users->random()->id,
            'admin_updated_id' => $users->random()->id,
            'levels_above' => null,
            'levels_under' => null,
            'created_at'=> $this->faker->dateTimeBetween('-4 years', '-1 years'),
            'updated_at'=> $this->faker->dateTimeBetween('-1 years', 'now'),
        ];
    }
}
