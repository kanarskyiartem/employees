<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PositionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::all();

        return [
            'name' => $this->faker->unique()->jobTitle(),
            'admin_created_id' => $users->random()->id,
            'admin_updated_id' => $users->random()->id,
            'created_at'=> $this->faker->dateTimeBetween('-4 years', '-1 years'),
            'updated_at'=> $this->faker->dateTimeBetween('-1 years', 'now'),
        ];
    }
}
